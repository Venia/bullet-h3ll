﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BulletH3ll
{
    public class Drawable
    {
        public Vector2 position; //Drawn position
        protected Vector2 origin;
        public Rectangle boundingBox; //bounding box for collision.
        public Texture2D sprite;
        public int baseSpeed = 8; //Movement speed of craft. pixels/second.
        public float armor { get; set; }
        public int damage = 100;

        public Drawable(Vector2 v)
        {
            position = v;
            armor = 1f;
        }
        public virtual void Initialize() {
            LoadContent();
            boundingBox = new Rectangle((int)(position.X - sprite.Width / 2), (int)(position.Y - sprite.Height / 2), (int)(sprite.Width * .8), (int)(sprite.Height * .8));
        }

        public virtual void LoadContent() 
        {
        }

        public virtual void Update(GameTime gameTime) {
            origin = new Vector2(sprite.Width / 2, sprite.Height / 2);
            boundingBox = new Rectangle((int)(position.X - sprite.Width / 2), (int)(position.Y - sprite.Height / 2), sprite.Width, sprite.Height);
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            
        }

        public virtual void Collide(object collider, Drawable owner)
        {

        }
    }
}
