﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulletH3ll.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BulletH3ll.Weapons;

namespace BulletH3ll.GameObj
{
    public class FlyingSaucer : Enemy
    {
        double lastShot;
        Texture2D projSprite;
        int dir;


        public FlyingSaucer(Vector2 v, int d)
            : base(v)
        {
            sprite = BulletHell.Instance.Content.Load<Texture2D>("flyingSaucer");
            origin = new Vector2(sprite.Width / 2, sprite.Height / 2);
            baseSpeed = 4;
            lastShot = 0;
            projSprite = BulletHell.Instance.Content.Load<Texture2D>("redLaserRay");
            dir = d;
            armor = 4;
            damage = 100;
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            position.X += baseSpeed*dir;
            if ((gameTime.TotalGameTime.TotalSeconds - lastShot) > .5f)
            {
                BulletHell.Instance.getGameView().projectiles.Add(
                    new LaserProj(projSprite, -384, position, 
                        (float)(Math.PI / 2), 0.1f, this));
                lastShot = gameTime.TotalGameTime.TotalSeconds;
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }

        public override void Collide(object collider, Drawable owner)
        {
            int dama = 0;
            if (collider is Projectile) dama = ((Projectile)collider).damage;
            else if (collider is Drawable) dama = ((Drawable)collider).damage;
            armor -= dama;
            if (armor < 0) BulletHell.Instance.getGameView().Dead.Add(this);
            if (owner.GetType() == typeof(Player)) ((Player)owner).score++;
        }
    }
}
