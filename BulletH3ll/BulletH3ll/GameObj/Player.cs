﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BulletH3ll.Weapons;
using Microsoft.Xna.Framework.Audio;
using BulletH3ll.GameObj;
using BulletH3ll.Views;

namespace BulletH3ll
{
    public class Player : Drawable
    {
        public PlayerIndex playerIndex;
        private InputHandler _inputHandler;
        public Weapon currentWeapon;
        public int speed;
        SoundEffect tick;
        public int score;

        public InputHandler inputHandler {
            get { //Lazily instantiate inputHandler perhaps?
                return _inputHandler;
            }
        }
        private float _armor;
        new public float armor
        {
            get
            {
                return _armor;
            }

            set
            {
                if ((value) >= 14) { _armor = 14; return; }
                if (value-_armor < 0) tick.Play();
                _armor = value;
            }
        }

        private Color tint;

        public Player(Vector2 v, PlayerIndex p) :base(v) {
            playerIndex = p;
            Initialize();
            _inputHandler = new InputHandler(playerIndex);
            switch((int)playerIndex) {
                case 0:
                    currentWeapon = new EnergyBeam(this);
                    break;
                case 1:
                    currentWeapon = new Laser(this);
                    break;
            }
            speed = baseSpeed;
            tick = BulletHell.Instance.Content.Load<SoundEffect>("tick");
            score = 0;
            armor = 14;
        }

        public override void Initialize()
        {
            LoadContent();
            switch (playerIndex) {
                case PlayerIndex.One:
                    tint = Color.Aqua;
                    break;
                case PlayerIndex.Two:
                    tint = Color.Pink;
                    break;
                default:
                    tint = Color.White;
                    break;
            }
        }

        public void associateKeyEvents()
        {

        }

        public override void LoadContent()
        {
            //Load Sprite Textures. We'll tint these by player data.
            sprite = BulletHell.Instance.Content.Load<Texture2D>("ship");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            //various non-button input related things.
            //joystick - movement per second based off gametime and movespeeed
            Vector2 joystick = GamePad.GetState(playerIndex).ThumbSticks.Left;
            position += (new Vector2((float)(speed * joystick.X), (float)(speed * joystick.Y * -1f)));

            //trigger (if on, shooting, if off, nothing)
            if (inputHandler.checkTrigger(TriggerType.Right, 0.5f))
            {
                currentWeapon.Shoot(gameTime);
            }
            else 
                currentWeapon.shooting = false;

            //Check for button events.
            inputHandler.checkEvents();

            base.Update(gameTime);
            boundingBox = new Rectangle((int)(position.X - sprite.Width / 2 + (int)(sprite.Width * .5) / 2 ), (int)(position.Y - sprite.Height / 2), (int)(sprite.Width * .5), (int)(sprite.Height * .5));
            BulletHell.Instance.getGameView().scores[(int)playerIndex] = score;
        }
            
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, null, tint, 0f, origin, 0.5f, SpriteEffects.None, 0);
            base.Draw(gameTime, spriteBatch);
        }

        public override void Collide(object collider, Drawable owner)
        {
            armor--;
            if (armor < 0)
            {
                BulletHell.Instance.players[(int)playerIndex] = null;
                int[] scores = BulletHell.Instance.getGameView().scores;
                BulletHell.Instance.viewController.pushView(
                    new EndGameView(scores));
            }
        }

        public void Die()
        {
            //push high score screen if there are no players left.
        }
    }
}
