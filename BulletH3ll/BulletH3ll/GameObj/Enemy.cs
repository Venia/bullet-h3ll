﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

//An enemy doesn't neccesarily have to be a single ship,
//the engine just sees an object that takes care of drawing and updating itself,
//so it can be a "wrapper" object that controls the movements of several 
//ships so they move as one.
//This allows Galaga and Space Invaders type AI.

namespace BulletH3ll.Views
{
    public class Enemy : Drawable
    {
        public float armor;

        public Enemy(Vector2 v)
            : base(v)
        {
            origin = Vector2.Zero;
            armor = 0f;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, null, Color.White, 0f, origin, 1.5f, SpriteEffects.None, 0);
        }

        public override void Collide(object collider, Drawable owner)
        {
            base.Collide(collider, owner);
        }
    }
}
