﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BulletH3ll.GameObj
{
    public class Projectile
    {
        public delegate int PositionBehaviorDelegate(float y);
        public Texture2D sprite;
        public int velocity;
        public Vector2 position;
        protected PositionBehaviorDelegate del;
        protected float rotation;
        protected Vector2 origin;
        protected float scale;
        public Rectangle boundingBox;
        public Drawable owner;
        public int damage { get; set; }

        public Projectile(Texture2D s, int v, Vector2 p, PositionBehaviorDelegate d, 
            float r, float scal, Drawable o)
        {
            sprite = s;
            velocity = v;
            position = p;
            del = d;
            rotation = r;
            origin = new Vector2(sprite.Width / 2, sprite.Height / 2);
            rotation = r;
            scale = scal;
            owner = o;
        }

        public virtual void Update(GameTime gameTime) {
            //This function is intended for generic projectiles.
            if (del == null) return;
            float y = position.Y + 
                (float)Math.Ceiling((float)velocity * 
                ((float)gameTime.ElapsedGameTime.Milliseconds / 1000f));
            position = new Vector2(del.Invoke(y), y);
            boundingBox = new Rectangle((int)(position.X - ((sprite.Width * scale) / 2)), (int)(position.Y - ((sprite.Height * scale) / 2)), (int)(sprite.Width * scale), (int)(sprite.Height * scale));

        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            spriteBatch.Draw(sprite, position, null, Color.White, rotation, origin, scale, SpriteEffects.None, 0);
        }

        public virtual void Collide() {
        }
    }
}
