﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BulletH3ll.Views
{
    public class HighScoreView : View
    {
        List<highScore> HighScores;
        InputHandler[] ih = new InputHandler[2];
        int[][] menuLoc = new int[2][] {
            new int[3],
            new int[3]
        }; //jagged array for array of arrays
        int[] xIndex = new int[2];
        SpriteFont font;
        double lastTime = 0f;
        int[] scores;
        ButtonState lastState;

        public HighScoreView() : base()
        {
        }

        public override void Initialize()
        {
            font = BulletHell.Instance.Content.Load<SpriteFont>("sfont");
            StreamReader sr = new StreamReader("scores.txt");
            string stream = sr.ReadToEnd();
            char[] nl = { '\r', '\n' };
            char[] delims = { ',' };
            string[] tokens;
            HighScores = new List<highScore>();
            foreach(string str in stream.Split(nl)) {
                tokens = str.Split(delims);
                if (tokens.Count() != 2) continue;
                HighScores.Add(new highScore(tokens[0], Convert.ToInt32(tokens[1]))); 
            }
            sr.Close();


        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Released && lastState == ButtonState.Pressed) 
                BulletHell.Instance.viewController.popView();
            lastState = GamePad.GetState(PlayerIndex.One).Buttons.A;
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            if (HighScores.Count() > 0)
            {
                for (int i = 0; i < HighScores.Count(); i++)
                {
                    spriteBatch.DrawString(font, 
                        (HighScores[i].name + "       " + HighScores[i].score.ToString()), 
                        new Vector2(400, 100+20*i), Color.White);
                }
            }  
        }
    }
}
