﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulletH3ll.GameObj;
using Microsoft.Xna.Framework.Media;

namespace BulletH3ll.Views
{
    public class GameView : View
    {
        public List<Enemy> enemies;
        public List<Projectile> projectiles;
        public List<object> Dead;
        double lastTime = 0;
        Random rand;
        public int[] scores = new int[2];
        public GameView()
            : base()
        {
            rand = new Random();
        }

        public override void Initialize()
        {
            //Add a controller for game objects.
            enemies = new List<Enemy>();
            projectiles = new List<Projectile>();
            Dead = new List<object>();
            song = BulletHell.Instance.Content.Load<Song>("ChaozJapan");
            MediaPlayer.Play(song);
            MediaPlayer.Volume = 0.5f;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (MediaPlayer.State != MediaState.Playing) MediaPlayer.Play(song);
            try
            {
                if ((gameTime.TotalGameTime.TotalSeconds - lastTime) > 3)
                {
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(-100, 100), 1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(-200, 100), 1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(-400, 100), 1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(-500, 100), 1));

                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(868, 100), -1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(968, 100), -1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(1168, 100), -1));
                    enemies.Add(new FlyingSaucer(new Microsoft.Xna.Framework.Vector2(1269, 100), -1));
                    lastTime = gameTime.TotalGameTime.TotalSeconds;

                    foreach (Player player in BulletHell.Instance.players)
                    {
                        if (player == null) continue;
                        player.armor--;
                    }
                }

                enemies.RemoveAll((x) =>
                {
                    if (x.position.X < -1000 || x.position.X > 1200) return true;
                    return false;
                });

                foreach (Projectile projectile in projectiles)
                {
                    projectile.Update(gameTime);
                }
                foreach (Enemy enemy in enemies)
                {
                    enemy.Update(gameTime);
                }
                foreach (Player player in BulletHell.Instance.players)
                {
                    if (player == null) continue;
                    player.Update(gameTime);
                }

                //Calculate collisions
                foreach (Projectile projectile in projectiles)
                {
                    foreach (Player player in BulletHell.Instance.players)
                    {
                        if (player == null) continue;
                        if (projectile.owner.GetType() == typeof(Player)) break;
                        if (projectile.owner == player) break;
                        if (projectile.boundingBox.Intersects(player.boundingBox))
                        {
                            player.Collide(projectile, projectile.owner);
                            projectile.Collide();
                        }
                    }
                    foreach (Enemy enemy in enemies)
                    {
                        if (projectile.owner == enemy) continue;
                        if (projectile.boundingBox.Intersects(enemy.boundingBox))
                        {
                            enemy.Collide(projectile, projectile.owner);
                            projectile.Collide();
                        }
                    }
                }

                projectiles.RemoveAll((x) =>
                {
                    if (x.position.Y > 2000 || x.position.Y < -1000) return true;
                    return false;
                });

                foreach (object dead in Dead)
                {
                    if (dead is Enemy) enemies.Remove((Enemy)dead);
                    else if (dead is Projectile) projectiles.Remove((Projectile)dead);
                }
            }
            catch (NullReferenceException n)
            {
            }
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            foreach (Projectile projectile in projectiles)
            {
                projectile.Draw(gameTime, spriteBatch);
            }
            foreach (Enemy enemy in enemies)
            {
                enemy.Draw(gameTime, spriteBatch);
            }
            foreach (Player player in BulletHell.Instance.players)
            {
                if (player == null) continue;
                player.Draw(gameTime, spriteBatch);
            }
        }
    }
}
