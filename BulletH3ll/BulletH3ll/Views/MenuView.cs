﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BulletH3ll.Views
{
    class MenuView : View
    {
        string[] menuEntries;
        int activeItem = 0;
        private SpriteFont font;
        private Texture2D pixel;
        private bool bol = true;
        public Vector2 lastState;
        Texture2D logo;

        public MenuView()
            : base()
        {
        }
        
        public override void Initialize()
        {
            menuEntries = new string[3] {"Play 1P.", "Player 2P", "High Scores"};
            LoadContent();
            //lastChange = new GameTime();
        }

        public void LoadContent()
        {
            font = BulletHell.Instance.Content.Load<SpriteFont>("sfont");
            pixel = new Texture2D(BulletHell.Instance.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White }); // so that we can draw whatever color we want
            logo = BulletHell.Instance.Content.Load<Texture2D>("bullethell");
        }

        public void associateKeyEvents()
        {
            BulletHell.Instance.players[0].inputHandler.AddEvent(ButtonType.A, menuPress, this);
        }

        public void menuPress(ButtonType b, Player p)
        {
            switch (activeItem) {
                case 0:
                    BulletHell.Instance.players[0].position = new Vector2(350, 500f);
                    BulletHell.Instance.players[1] = null;
                    BulletHell.Instance.viewController.pushView(new GameView());
                    break;
                case 1:
                    BulletHell.Instance.players[0].position = new Vector2(300f, 500f);
                    BulletHell.Instance.players[1] =
                        new Player(new Vector2(400f, 500f), PlayerIndex.Two);
                    BulletHell.Instance.viewController.pushView(new GameView());
                    break;
                case 2:
                    BulletHell.Instance.viewController.pushView(new HighScoreView());
                    break;

            }
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (bol == true)
            {
                associateKeyEvents();
                bol = false;
            }
            #region 
            /*switch (BulletHell.Instance.players[0].inputHandler.GetThumbstickDirection(true))
            {
                case Buttons.DPadUp:
                    if ((gameTime.TotalGameTime - lastChange.TotalGameTime).Milliseconds > 500)
                    {
                        activeItem--;
                        if (activeItem < 0)
                            activeItem = menuEntries.Length - 1;
                        lastChange = gameTime;
                    }
                    break;
                case Buttons.DPadDown:
                    if ((gameTime.TotalGameTime - lastChange.ElapsedGameTime).Milliseconds > 500)
                    {
                        activeItem++;
                        if (activeItem > menuEntries.Length - 1)
                            activeItem = 0;
                        lastChange = gameTime;
                    }
                    break;
            }*/
            #endregion
            Vector2 thumbstick = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left;
            if ((thumbstick.Y > 0f) && (lastState.Y == 0))
            {
                activeItem--;
                if (activeItem < 0)
                    activeItem = menuEntries.Length - 1;
            }
            else if ((thumbstick.Y < 0f) && (lastState.Y == 0))
            {
                activeItem++;
                if (activeItem > menuEntries.Length - 1)
                    activeItem = 0;
            }
            lastState = thumbstick;
            try
            {
                BulletHell.Instance.players[0].inputHandler.checkEvents();
            }
            catch(NullReferenceException) {}
        }

/*        public void HandleDPad(ButtonType b, )
        {
            if (lastState.IsKeyDown(Keys.Up) && state.IsKeyUp(Keys.Up))
            {
                activeItem--;
                if (activeItem < 0)
                    activeItem = menuEntries.Length - 1;
            }

            if (lastState.IsKeyDown(Keys.Down) && state.IsKeyUp(Keys.Down))
            {
                activeItem++;
                if (activeItem > menuEntries.Length - 1)
                    activeItem = 0;
            }
        }*/

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            //Draw menu items.
            for (int i = 0; i < menuEntries.Length; i++)
            {
                spriteBatch.DrawString(font, menuEntries[i], new Vector2(400, 250+(50*i)), Color.White);
            }
            DrawBorder(new Rectangle(350, 240 + activeItem * 50, 250, 50), 2, Color.White, spriteBatch);
            spriteBatch.Draw(logo, new Vector2(250, 50), Color.White);
        }

        //Not my function, curtesy of BlueLine Game Studios.
        private void DrawBorder(Rectangle rectangleToDraw, int thicknessOfBorder, Color borderColor, SpriteBatch spriteBatch)
        {
            // Draw top line
            spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, rectangleToDraw.Width, thicknessOfBorder), borderColor);

            // Draw left line
            spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, thicknessOfBorder, rectangleToDraw.Height), borderColor);

            // Draw right line
            spriteBatch.Draw(pixel, new Rectangle((rectangleToDraw.X + rectangleToDraw.Width - thicknessOfBorder),
                                            rectangleToDraw.Y,
                                            thicknessOfBorder,
                                            rectangleToDraw.Height), borderColor);
            // Draw bottom line
            spriteBatch.Draw(pixel, new Rectangle(rectangleToDraw.X,
                                            rectangleToDraw.Y + rectangleToDraw.Height - thicknessOfBorder,
                                            rectangleToDraw.Width,
                                            thicknessOfBorder), borderColor);
        }
    }
}
