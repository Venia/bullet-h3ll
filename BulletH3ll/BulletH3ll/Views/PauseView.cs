﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BulletH3ll.Views
{
    class PauseView : View
    {
        private SpriteFont font;
        private Player player;
        bool bol = true;
        public PauseView(Player p)
            : base()
        {
            player = p;
        }

        public override void Initialize()
        {
            LoadContent();
        }

        public void associateKeyEvents()
        {
            player.inputHandler.AddEvent(ButtonType.Start, delegate(ButtonType b, Player pp)
            {
                BulletHell.Instance.viewController.popView();
            }, this);
        }

        public void LoadContent()
        {
            font = BulletHell.Instance.Content.Load<SpriteFont>("sfont");
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if(bol == true) { 
                associateKeyEvents();
                bol = false;
            }
            player.inputHandler.checkEvents();
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            //Draw menu items.
            spriteBatch.DrawString(font, "Game Paused", new Vector2(325, 225), Color.White);
        }
    }
}
