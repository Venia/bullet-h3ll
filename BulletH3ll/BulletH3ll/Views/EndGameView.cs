﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BulletH3ll.Views
{
    struct highScore
    {
        public string name;
        public int score;

        public highScore(string c, int s)
        {
            name = c;
            score = s;
        }
    }

    public class EndGameView : View
    {
        List<highScore> HighScores;
        InputHandler[] ih = new InputHandler[2];
        int[][] menuLoc = new int[2][] {
            new int[3],
            new int[3]
        }; //jagged array for array of arrays
        int[] xIndex = new int[2];
        SpriteFont font;
        double lastTime = 0f;
        int[] scores;

        public EndGameView(int[] i) : base()
        {
            if (i != null)
            {
                scores = i;
                BulletHell.Instance.viewController.popView();
                if (i[0] != null)
                    ih[0] = new InputHandler(Microsoft.Xna.Framework.PlayerIndex.One);
                if (i[1] != null)
                    ih[1] = new InputHandler(Microsoft.Xna.Framework.PlayerIndex.Two);
            }
        }

        public override void Initialize()
        {
            font = BulletHell.Instance.Content.Load<SpriteFont>("sfont");
            StreamReader sr = new StreamReader("scores.txt");
            string stream = sr.ReadToEnd();
            char[] nl = { '\r', '\n' };
            char[] delims = { ',' };
            string[] tokens;
            HighScores = new List<highScore>();
            foreach(string str in stream.Split(nl)) {
                tokens = str.Split(delims);
                if (tokens.Count() != 2) continue;
                HighScores.Add(new highScore(tokens[0], Convert.ToInt32(tokens[1]))); 
            }
            sr.Close();


        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if(gameTime.TotalGameTime.TotalSeconds - lastTime > .5)
            {
            foreach (InputHandler input in ih)
            {
                if (input == null) continue;
                switch (input.GetThumbstickDirection(true))
                {
                    case Microsoft.Xna.Framework.Input.Buttons.DPadRight:
                        xIndex[(int)input.pi]++;
                        if (xIndex[(int)input.pi] > 2)
                            xIndex[(int)input.pi] = 0;
                        break;
                    case Microsoft.Xna.Framework.Input.Buttons.DPadLeft:
                        xIndex[(int)input.pi]--;
                        if (xIndex[(int)input.pi] < 0)
                            xIndex[(int)input.pi] = 2;
                        break;
                    case Microsoft.Xna.Framework.Input.Buttons.DPadUp:
                        menuLoc[(int)input.pi][xIndex[(int)input.pi]]++;
                        if (menuLoc[(int)input.pi][xIndex[(int)input.pi]] > 24)
                            menuLoc[(int)input.pi][xIndex[(int)input.pi]] = 0;
                        break;
                    case Microsoft.Xna.Framework.Input.Buttons.DPadDown:
                        menuLoc[(int)input.pi][xIndex[(int)input.pi]]--;
                        if (menuLoc[(int)input.pi][xIndex[(int)input.pi]] < 0)
                            menuLoc[(int)input.pi][xIndex[(int)input.pi]] = 24;
                        break;
                }
                lastTime = gameTime.TotalGameTime.TotalSeconds;
            }
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed) Save();
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            if (HighScores.Count() > 0)
            {
                for (int i = 0; i < HighScores.Count(); i++)
                {
                    spriteBatch.DrawString(font, 
                        (HighScores[i].name + "       " + HighScores[i].score.ToString()), 
                        new Vector2(400, 100+20*i), Color.White);
                }
            }
            foreach (InputHandler input in ih)
            {
                if (input == null) continue;
                for (int i = 0; i < 3; i++)
                {
                    int chara = menuLoc[(int)input.pi][i] + 65;
                    spriteBatch.DrawString(font, Enum.GetName(typeof(Keys), chara), new Vector2(300 + (20 * i) + (200 * (int)(input.pi)), 600), Color.White);
                }
            }    
        }

        public void Save()
        {
            if (scores == null) return;
            string str = "";
            foreach (int i in menuLoc[0])
            {   
                str += (char)(i+65);
            }
            for (int i = 0; i < HighScores.Count(); i++)
            {
                if (scores[0] > HighScores[i].score) {
                    HighScores.Insert(i, new highScore(str,scores[0]));
                    break;
                }
            }
            string concatStr = "";
            foreach (highScore score in HighScores)
            {
                concatStr += score.name + "," + score.score.ToString() + '\r' + '\n';
            }
            StreamWriter sw = new StreamWriter("scores.txt");
            sw.Write(concatStr);
            sw.Close();

            BulletHell.Instance.viewController.popView();
        }
    }
}
