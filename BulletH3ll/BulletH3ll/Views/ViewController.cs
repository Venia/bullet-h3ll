﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace BulletH3ll
{
    public class ViewController
    {
        public View currentView
        {
            get
            {
                if (viewStack.Count() == 0) { return null; }
                return viewStack.Peek(); 
            }
        }
         
        private Stack<View> viewStack;
        protected GraphicsDeviceManager graphics;

        public ViewController(GraphicsDeviceManager g)
        {
            graphics = g;
            viewStack = new Stack<View>();
        }

        public void pushView(View v)
        {
            MediaPlayer.Stop();
            foreach (Player player in BulletHell.Instance.players)
            {
                if (player == null) continue;
                player.inputHandler.pushView(ref v);
            }
            viewStack.Push(v);
        }

        public View popView()
        {
            View v = viewStack.Pop();
            foreach (Player player in BulletHell.Instance.players)
            {
                if (player == null) continue;
                player.inputHandler.popView(v);
            }
            return v;
        }

        public View peekView()
        {
            return viewStack.Peek();
        }
    }
}
