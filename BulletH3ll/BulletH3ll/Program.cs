//Jonathan Howard - CS1302 - BulletH3ll
using System;

namespace BulletH3ll
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (BulletHell game = new BulletHell())
            {
                game.Run();
            }
        }
    }
#endif
}

