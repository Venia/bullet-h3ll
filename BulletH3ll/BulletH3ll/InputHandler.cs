﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Threading;

namespace BulletH3ll
{
    public enum TriggerType { Left, Right };
    public enum ButtonType { A, B, Back, LeftShoulder, RightShoulder, LeftStick, RightStick, Start, X, Y };
    //Delegates for input events.
    public delegate void PressOccured(ButtonType b, Player p);

    public struct ButtonEvent
    {
        public ButtonType button;
        public PressOccured keyEventDel;
        public ButtonEvent(ButtonType b, PressOccured p)
        {
            button = b;
            keyEventDel = p;
        }
    }

    public class InputHandler
    {
        public PlayerIndex pi;
        GamePadButtons lastState;
        Dictionary<string, List<ButtonEvent>> buttonEvents;

        public InputHandler(PlayerIndex p)
        {
            pi = p;
            lastState = GamePad.GetState(pi).Buttons;
            buttonEvents = new Dictionary<string,List<ButtonEvent>>();
        }
        
        public void AddEvent(ButtonType button, PressOccured p, View view) {
            buttonEvents[view.ToString()].Add(new ButtonEvent(button, p));
        }

        public void popView(View view)
        {
            buttonEvents.Remove(view.ToString());
        }

        public void pushView(ref View v)
        {
            buttonEvents[v.ToString()] = new List<ButtonEvent>();
        }

        //This is generated once for a player.
        public void checkEvents()
        {
            GamePadButtons currentState = GamePad.GetState(pi).Buttons;
            foreach (ButtonEvent eVent in buttonEvents[BulletHell.Instance.viewController.currentView.ToString()])
            {
                if (checkPress(eVent.button, currentState)) 
                    eVent.keyEventDel.Invoke(eVent.button, BulletHell.Instance.players[(int)pi]);
            }
            lastState = currentState;
        }

        //Check a specific button.
        public bool checkPress(ButtonType button, GamePadButtons currentState)
        {
            switch (button)
            {
                case ButtonType.A:
                    return (lastState.A == ButtonState.Pressed &&
                        currentState.A == ButtonState.Released);
                case ButtonType.B:
                    return (lastState.B == ButtonState.Pressed &&
                        currentState.B == ButtonState.Released);
                case ButtonType.LeftShoulder:
                    return (lastState.LeftShoulder == ButtonState.Pressed &&
                        currentState.LeftShoulder == ButtonState.Released);
                case ButtonType.RightShoulder:
                    return (lastState.RightShoulder == ButtonState.Pressed &&
                        currentState.RightShoulder == ButtonState.Released);
                case ButtonType.LeftStick:
                    return (lastState.LeftStick == ButtonState.Pressed &&
                        currentState.LeftStick == ButtonState.Released);
                case ButtonType.RightStick:
                    return (lastState.RightStick == ButtonState.Pressed &&
                        currentState.RightStick == ButtonState.Released);
                case ButtonType.Start:
                    return (lastState.Start == ButtonState.Pressed &&
                        currentState.Start == ButtonState.Released);
                case ButtonType.X:
                    return (lastState.X == ButtonState.Pressed &&
                        currentState.X == ButtonState.Released);
                case ButtonType.Back:
                    return (lastState.Back == ButtonState.Pressed &&
                        currentState.Back == ButtonState.Released);
                default:
                    throw (new ArgumentException());
            }
        }

        public bool checkTrigger(TriggerType t, float s) {
            switch (t) {
                case TriggerType.Left:
                    return (GamePad.GetState(pi).Triggers.Left > s);
                case TriggerType.Right:
                    return (GamePad.GetState(pi).Triggers.Right > s);
                default:
                    throw (new ArgumentException());
            }
         }

        public Buttons GetThumbstickDirection(bool leftStick)
        {
            float thumbstickTolerance = 0.35f;

            GamePadState gs = GamePad.GetState(pi);
            Vector2 direction = (leftStick) ?
                gs.ThumbSticks.Left : gs.ThumbSticks.Right;

            float absX = Math.Abs(direction.X);
            float absY = Math.Abs(direction.Y);

            if (absX > absY && absX > thumbstickTolerance)
            {
                return (direction.X > 0) ? Buttons.DPadRight : Buttons.DPadLeft;
            }
            else if (absX < absY && absY > thumbstickTolerance)
            {
                return (direction.Y > 0) ? Buttons.DPadUp : Buttons.DPadDown;
            }
            return (Buttons)0;
        }
 
    }
}
