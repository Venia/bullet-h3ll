﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulletH3ll.GameObj;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BulletH3ll.Weapons
{
    class LaserProj : Projectile 
    {
        public LaserProj(Texture2D s, int v, Vector2 p, float r, float scal, Drawable o)
            : base(s,v,p,null,r,scal,o)
        {
            damage = 2;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            //Do not call base Update, since we're implementing custom animations &
            //linear projectile motion.

            position.Y -= (float)(velocity * gameTime.ElapsedGameTime.TotalSeconds);
            boundingBox = new Rectangle((int)(position.X - ((sprite.Width * scale) / 2)), (int)(position.Y - ((sprite.Height * scale) / 2)), (int)(sprite.Width * .5 * scale), (int)(sprite.Height * .8 * scale));
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }

        public override void Collide()
        {
            BulletHell.Instance.getGameView().Dead.Add(this);
            base.Collide();
        }
    }
}
