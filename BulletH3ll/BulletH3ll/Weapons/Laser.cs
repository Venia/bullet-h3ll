﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using BulletH3ll.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace BulletH3ll.Weapons
{
    public class Laser : Weapon 
    {
        double lastShot;
        SoundEffect sound;

        public Laser(Player p)
            : base(p)
        {
            projSprite = BulletHell.Instance.Content.Load<Texture2D>("redLaserRay");
            sound = BulletHell.Instance.Content.Load<SoundEffect>("laser");
        }

        public override void Shoot(GameTime gameTime)
        {
            GameView view = BulletHell.Instance.getGameView();
            if (view == null) return;

            if(gameTime.TotalGameTime.TotalSeconds - lastShot > .1) {
                view.projectiles.Add(new LaserProj(projSprite, 384, player.position, (float)(Math.PI/2), 0.1f, player));
                lastShot = gameTime.TotalGameTime.TotalSeconds;
                sound.Play(.25f, 0f, 1f);
            }   
        }
    }
}