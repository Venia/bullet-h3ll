﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulletH3ll.GameObj;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace BulletH3ll.Weapons
{
    public class EnergyBeam : Weapon
    {
        public Texture2D sprite;
        public SoundEffectInstance sinstance;
        bool _shooting;
        EnergyBeamProj _projectile;
        float lastTime;

        protected EnergyBeamProj projectile {
            get {
                //Crazy thing to prevent populating the projectiles list with a lot
                //of the same thing -> 
                if(_projectile == null) _projectile = 
                    (EnergyBeamProj)((BulletHell.Instance.getGameView().projectiles.Find(
                    (x) => { return (x is EnergyBeamProj); }))
                    ?? new EnergyBeamProj(
                    BulletHell.Instance.Content.Load<Texture2D>("energybeam"),
                    0, player.position, 0f, 0f, player));
                return _projectile;
            }
        }

        public override bool shooting
        {
            get { return _shooting; }
            set
            {
                if (value == false)
                {
                    sinstance.Pause();
                    projectile.shooting = false;
                    //BulletHell.Instance.getGameView().projectiles.Remove(projectile);
                }
                _shooting = value;
            }
        }

        public EnergyBeam(Player p)
            : base(p)
        {
            SoundEffect sound = BulletHell.Instance.Content.Load<SoundEffect>("beamLoop");
            sinstance = sound.CreateInstance();
            sinstance.IsLooped = true;
            sinstance.Volume = .2f;
            //shooting = false;
            //Check if an projectile already exists, if not, create a new one.
        }

        public override void Shoot(GameTime gameTime)
        {
            if (shooting == false)
            {
                sinstance.Play();
                shooting = true;
                if((BulletHell.Instance.getGameView().projectiles.Find(
                    (x) => { return (x is EnergyBeamProj); }) == null)) 
                    BulletHell.Instance.getGameView().projectiles.Add(projectile);
                projectile.position = player.position;
                projectile.shooting = true;
            }
            base.Shoot(gameTime);
        }
    }

    public class EnergyBeamProj : Projectile
    {
        public Rectangle viewBox;
        public bool shooting;

        public EnergyBeamProj(Texture2D s, int v, Vector2 p,
            float r, float scal, Drawable o)
            : base(s, v, p, null, r, scal, o)
        {
            damage = 1;
            position = o.position;
            sprite = BulletHell.Instance.Content.Load<Texture2D>("energybeam");
            origin = new Vector2(68, 55);
            scale = 1f;
            rotation = (float)(Math.PI);
            boundingBox = new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
            owner = o;
            velocity = 0;
            viewBox = new Rectangle(0, 0, sprite.Width, 100);
        }

        public override void Update(GameTime gameTime)
        {
            position = owner.position;
            if (shooting) {
                ((Player)owner).speed = owner.baseSpeed / 2;
                if (viewBox.Height < sprite.Height) viewBox.Height += 10; }
            else {
                ((Player)owner).speed = owner.baseSpeed;
                if (viewBox.Height > 0) viewBox.Height -= 10; 
            }
            boundingBox = new Rectangle((int)(position.X - 68), (int)(sprite.Height - viewBox.Height), sprite.Width, viewBox.Height);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, viewBox, Color.White, rotation, origin, scale, SpriteEffects.None, 0);
        }
    }
}
