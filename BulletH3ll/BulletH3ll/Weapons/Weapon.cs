﻿//Jonathan Howard - CS1302 - BulletH3ll
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BulletH3ll.Weapons
{
    public class Weapon
    {
        public Player player;
        public Texture2D projSprite;
        public virtual bool shooting { get; set; }

        public Weapon(Player p)
            : base()
        {
            player = p;
        }

        public virtual void Shoot(GameTime gameTime)
        {

        }
    }
}
